package challange2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class math {
    public float rata(ArrayList<Integer> semuaNilai) {
        float rata;
        int total = 0;
        int banyakData = semuaNilai.size();

        for (int nilai : semuaNilai) {
            total += nilai;
        }

        rata = total / banyakData;

        return rata;
    }
    public int median(ArrayList<Integer> semuaNilai) {
        int median = 0;

        this.sort(semuaNilai);
        for (int i = 0; i < semuaNilai.size(); i++) {
            semuaNilai.get(i);
        }
        int tengah = semuaNilai.size() / 2;
        if (semuaNilai.size() % 2 == 1) {
            median = semuaNilai.get(tengah);
        } else {
            median = (semuaNilai.get(tengah - 1) + semuaNilai.get(tengah)) / 2;
        }

        return median;
    }

    public int modus(ArrayList<Integer> semuaNilai) {
        int modus = 0;
        int max=0;
        for(int i=0;i< semuaNilai.size();++i){
            int count = 0;
            for(int j=0;j< semuaNilai.size();++j){
                if(semuaNilai.get(j) == semuaNilai.get(i))
                count++;
            }
            if(count>max){
                max = count;
                modus = semuaNilai.get(i);
            }
        }
        return modus;
    }
    private void sort(ArrayList<Integer> semuaNilai) {
        Collections.sort(semuaNilai);
    }
}
